# Rockyou 2021 reb00t

Original Post: https://raidforums.com/Thread-82-billion-rockyou2021-passward-list-dictionary

This combines all the following passwords lists:

* https://crackstation.net/crackstation-wordlist-password-cracking-dictionary.htm
* https://www.hack3r.com/forum-topic/wikipedia-wordlist
* https://github.com/danielmiessler/SecLists/tree/master/Passwords
* https://github.com/berzerk0/Probable-Wordlists
* https://weakpass.com/download

All passwords are 6-20 characters long, all lines with non-ASCII characters or white space or tab are removed, resulting in 82 billion unique entries. You are also welcome to add to it and share.

## Installation

```bash
git clone https://gitlab.com/5amu/rockyou2021
make prepare
sudo make install
```
